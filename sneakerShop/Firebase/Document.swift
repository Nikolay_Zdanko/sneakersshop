//
//  Document.swift
//  sneakerShop
//
//  Created by Николай on 8.08.21.
//

import Foundation
import UIKit

struct Document {
    let name: String
    let field1: String
    let field2: String
    let image: UIImage?
}
