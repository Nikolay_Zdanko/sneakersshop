//
//  ViewController.swift
//  sneakerShop
//
//  Created by Николай on 7.08.21.
//

import UIKit
import Firebase
import FirebaseStorage

class ViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
//    var arrayModel: [Firestore] = []
    var arrayLabe1: [Document] = []
    var arrayLabe2: [String] = []
    var images: [UIImage] = []
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
//        setup()
        
        collectionView.register(CollectionViewCell.nib(), forCellWithReuseIdentifier: CollectionViewCell.identifier)
        
        let storage = Storage.storage()
        let reference = storage.reference()
        let pathRef = reference.child("pictures")
        
        var image: UIImage = UIImage(named: "defaultPic")!
        
//        let fileRef = pathRef.child(picName + ".jpeg")
//        fileRef.getData(maxSize: 1024*1024) { data, error in
//            guard  error == nil else { completion(image); return }
//            image = UIImage(data: data!)!
//            completion(image)
//
            
        let db = configureFB()
        db.collection("sneaker").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
//                    print("\(document.documentID) => \(document.data())")
                    for (name, price) in document.data() {
//                        print(document.data().keys)
                        if name == "field1" {
                            
                            let doc =  Document(name: document.documentID, field1: price as! String , field2: document.documentID, image: nil)
                                 self.arrayLabe1.append(doc)
                            
                        }
                       
                        
                            
                    }
                   
//                    for (name, price) in document.data() {
////                        print("name: \(name), price: \(price)")
//                        if price as! String == "field1" {
//                            self.arrayLabe1.append(price as! String)
//
//                        }
                        
//                        self.arrayLabe2.append(price as! String)
                        
                    }
                
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }
            }
        }
        
    }

    func configureFB() -> Firestore {
        var db: Firestore!
        let settings = FirestoreSettings()
        Firestore.firestore().settings = settings
        db = Firestore.firestore()
        return db
    }
    
    
    
//    func setup() {
//        APIManager.shared.getPost(collection: "sneaker", docName: "nike") { doc in
//                    guard doc != nil else { return }
//            self.arrayLabe1.append(doc?.field1 ?? String())
//                    self.arrayLabe1.text = doc?.field1
//                    self.label2.text = doc?.field2
//                }
//                APIManager.shared.getImage(picName: "airJordan1") { pic in
////                    self.imageView.image = pic
//                    self.images.append(pic)
//                    self.collectionView.reloadData()
//                }
//        APIManager.shared.getImage { pic in
////            print("111 - \(pic)")
//            self.images.append(pic)
//            self.collectionView.reloadData()
//        }
    
    }
    

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayLabe1.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath) as? CollectionViewCell else { return UICollectionViewCell() }
//        let imagesSneaker = images[indexPath.item]
//        cell.imageView.image = imagesSneaker
        let collections = arrayLabe1[indexPath.item]
        cell.modelLabel.text = collections.name
        cell.priceLabel.text = collections.field1
//        cell.priceLabel.text = arrayLabe2[indexPath.item]

        return cell
    }
    
    
}
